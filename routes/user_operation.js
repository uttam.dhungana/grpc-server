const db = require('../config');

//Get User List
exports.getUsers = async function (call, callback) {
    await db.query('SELECT * FROM users', (err, rows) => {
        if (err) throw err;
        console.log({ users: rows });
        callback(null, { users: rows });
    });
};

exports.getUser = async function (call, callback) {
    let id = call.request.id;
    await db.query('SELECT * FROM users WHERE id = ?', [id], (err, [rows]) => {
        if (err) throw err;
        console.log(rows);
        callback(null, rows);
    });
};

exports.saveUser = async function (call, callback) {

    let data = {
        name: call.request.name,
        address: call.request.address,
        email: call.request.email,
        phone: call.request.phone,
        status: call.request.status,
    };

    await db.query("INSERT INTO users set ? ", data, (err, rows) => {
        if (err) callback(null, {
            code: 101,
            description: 'Insert Failed!',
        });
        callback(null, {
            code: 100,
            description: 'Inserted Successfully!',
            id: rows.insertId,
        });
    });
};

exports.editUser = async function (call, callback) {
    let id = call.request.id;
    let data = {
        name: call.request.name,
        address: call.request.address,
        email: call.request.email,
        phone: call.request.phone,
        status: call.request.status,
    };
    await db.query("UPDATE users set ? WHERE id = ? ", [data, id], (err, rows) => {
        if (err) callback(null, {
            code: 101,
            description: 'Update Failed!',
            id: id,
        });
        callback(null, {
            code: 100,
            description: 'Updated Successfully!',
            id: id,
        });
    });
};


exports.deleteUser = async function (call, callback) {
    let id = call.request.id;
    await db.query("DELETE FROM users  WHERE id = ? ", [id], function (err, rows) {
        if (err) callback(null, {
            code: 101,
            description: 'Dalete Failed!',
            id: id,
        });
        callback(null, {
            code: 100,
            description: 'Deleted Successfully!',
            id: id,
        });
    });
};
