const grpc = require('grpc');
const protoLoader = require('@grpc/proto-loader');

const PROTO_PATH = './proto/user.proto';
const packageDefinition = protoLoader.loadSync(PROTO_PATH, {
  keepCase: true,
  longs: 'string',
  defaults: true
});
const proto = grpc.loadPackageDefinition(packageDefinition);

// const users = [
//   { id: '1', name: 'John Doe', address: '1 Sterling Way, CA 45532' },
//   { id: '2', name: 'Jane Doe', address: '2 Sterling Way, CA 45532' }
// ]

const server = new grpc.Server();

var userOperation = require('./routes/user_operation');

async function List(call, callback) {
  const users = await userOperation.getUsers(call, callback);
  callback(null, users);
}

async function Insert(call, callback) {
  const user = await userOperation.saveUser(call, callback);
  callback(null, user);
}

async function Delete(call, callback) {
  const user = await userOperation.deleteUser(call, callback);
  callback(null, user);
}

async function GetUser(call, callback) {
  console.log(call.request);
  const user = await userOperation.getUser(call, callback);
  callback(null, user);
}

async function Update(call, callback) {
  const user = await userOperation.editUser(call, callback);
  callback(null, user);
}


server.addService(proto.user.UserService.service, {
  List,
  Insert,
  Delete,
  GetUser,
  Update
});

server.bind('0.0.0.0:50051', grpc.ServerCredentials.createInsecure());
console.log('Server running at http://0.0.0.0:50051');
server.start();
if (server.started) {
  console.log('server started....=======>>>>>>');
}
