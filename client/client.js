const grpc = require('grpc');
const protoLoader = require('@grpc/proto-loader');
const path = require('path');
const PROTO_PATH = path.join('./proto/user.proto');
const packageDefinition = protoLoader.loadSync(PROTO_PATH, {
  keepCase: true,
  longs: 'string',
  defaults: true
});

const proto = grpc.loadPackageDefinition(packageDefinition);

const client = new proto.user.UserService('0.0.0.0:50051',
  grpc.credentials.createInsecure());

module.exports = client;